FROM nginx:alpine

COPY ./conf.d /etc/nginx/conf.d
COPY ./html /usr/share/nginx/html
COPY ./lafollehistoireduculdaethesin /usr/share/nginx/lafollehistoireduculdaethesin

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
